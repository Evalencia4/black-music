import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ArtistaComponent } from '../artista/artista.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html',
  styleUrls: ['./tarjetas.component.css']
})
export class TarjetasComponent {

  @Input() items: any[] = [];

  constructor( private router: Router, private http:HttpClient ) { 

                
    
  }
  
  verArtista( item: any ) {

    let artistaId;

    if ( item.type === 'artist' ) {
      artistaId = item.id;
    } else {
      artistaId = item.artists[0].id;
    }

    this.router.navigate([ '/artist', artistaId  ]);

  }

  prueba(artista:any){
    let artistaI; 
    let nameA;
    let urlImagen;
   if ( artista.type === 'artist' ) {
    artistaI = artista.id;
  } else {
    artistaI = artista.artists[0].id;
  }

  if ( artista.type === 'artist' ) {
    nameA = artista.name;
  } else {
    nameA = artista.artists[0].name;
  }
  
  if ( artista.type === 'artist' ) {
    urlImagen = artista.images[0].url;
  } else {
    urlImagen = artista.images[0].url;
  }
  


 
  let url='http://webapisd.azurewebsites.net/api/insert';
  let jsone ={'id':artistaI,'name':nameA,'imagen':urlImagen} 
  let response= this.http.post(url,jsone).subscribe(data => {
    // Entra aquí con respuesta del servicio correcta código http 200
    console.log(data)
}, err => {
    // Entra aquí si el servicio entrega un código http de error EJ: 404, 
    
    console.log(err)
})
  
  console.log(artistaI);
  console.log(nameA);
  console.log(artista);
  console.log(urlImagen);
  
       }
}
