import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {

  access_token: any[] = [];
  constructor(private spoty:SpotifyService) { }

  ngOnInit() {
  }
  getToken() {
    var token=this.spoty.getToken().subscribe((data:any)=>{
      this.access_token = data;
    
    console.log(data);
       return data

    });
    
   
  }



}
