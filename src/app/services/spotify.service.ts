import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class SpotifyService {


  constructor(private http: HttpClient) {
    console.log('Spotify Service Listo');
  }

  getToken() {
    const url='https://accounts.spotify.com/api/token';
    const headers= new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded'});
    const body =  {'grant_type':'client_credentials','client_id':'ff1329cdefda4af48c92065a39e6efa8','client_secret':'dd830908104940bdac2220f5879e700e'};
    var token=this.http.post(url,body,{headers});
    return token
  }

  getQuery( query: string ) {

    const url = `https://api.spotify.com/v1/${ query }`;

    const headers = new HttpHeaders({
      'Authorization': 'Bearer BQDHLaJ_ucgjB5Rw8e7rVDTL3o_bCOLMVEL4ysKz_D2NiNuqDyimCfyPp1b7MKbEehYkGTIOleQYIVQr7a0'
    });

    return this.http.get(url, { headers });

  }


  getNewReleases() {

    return this.getQuery('browse/new-releases?limit=20')
              .pipe( map( data => data['albums'].items ));

  }

  getArtistas( termino: string ) {

    return this.getQuery(`search?q=${ termino }&type=artist&limit=15`)
                .pipe( map( data => data['artists'].items));

  }

  getArtista( id: string ) {

    return this.getQuery(`artists/${ id }`);
                // .pipe( map( data => data['artists'].items));

  }

  getTopTracks( id: string ) {

    return this.getQuery(`artists/${ id }/top-tracks?country=us`)
                .pipe( map( data => data['tracks']));

  }

}